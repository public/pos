#ifndef _DB_IF_H_
#define _DB_IF_H_
#include "posdb.h"
#include "salt_store.h"
#include "sha1.h"
#include <string>
#include <list>

class DbIf {
public:
	DbIf(int argc, char ** argv);
	~DbIf();
	std::string getSalt();
	bool consumeKey(std::string key);

	PosDb & getDb();

private:
	PosDb * db;
	SaltStore * saltStore;

};

#endif

