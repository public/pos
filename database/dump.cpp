#include <stdio.h>
#include <unistd.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include "Pos.h"
#include "sha1.h"
#include <iostream>

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

using namespace pos;

using namespace boost;

int main(int argc, char** argv) {
  if (argc != 2) {
    cout << "Usage: dump <host>\n";
    exit(1);
  }
  shared_ptr<TTransport> socket(new TSocket(argv[1], 9090));
  shared_ptr<TTransport> transport(new TFramedTransport(socket));
  shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
  PosClient client(protocol);

  try {
    transport->open();
  } catch(TException & e) {
    std::cerr << e.what() << "\n";
    exit(1);
  }

  vector<string> data;
  client.toString(data);
  for (uint32_t i = 0; i < data.size(); i++) {
    cout << data[i] << "\n";
  }
  return 0;
}

