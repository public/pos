#ifndef _POS_LOG_H_
#define _POS_LOG_H_
#include "common.h"
#include "sha1.h"
#include <list>
#include <string>
#include <vector>

enum E_OBJ_TYPE { ET_TRANS, ET_HASH, ET_SALE, ET_PRICE, ET_ADDUPC, ET_REVERT };

struct __attribute__ ((aligned (64), packed)) LogEntry
{
	uint64_t ts;
	uint64_t serial;
	E_OBJ_TYPE type;
	union {
		struct __attribute__ ((packed)) {
			uint32_t uid;
			int32_t delta;
		} Transaction;

		struct __attribute__ ((packed)) {
			char hash[20];
			uint32_t uid;
			bool add;
		} HashChange;

		struct __attribute__ ((packed)) {
			uint64_t upc;
			int32_t delta;
		} StockChange;

    struct __attribute__ ((packed)) {
      uint64_t upc;
      int32_t price;
    } PriceChange;

    struct __attribute__ ((packed)) {
      uint64_t upc;
      char name[33];
    } AddUPC;

		struct __attribute__ ((packed)) {
			uint64_t revert_serial;
		} Revert;
	};
};

class Log {
public:
	Log(const char * fn);
	uint64_t writeEntry(LogEntry ent);
	uint64_t nextSerial();
  LogEntry newLogEntry(E_OBJ_TYPE type);

  std::vector<std::string> toString();


	std::list<LogEntry>::const_iterator begin();
	std::list<LogEntry>::const_iterator end();

private:
	std::list<LogEntry> entries;
	std::string log_name;
	uint64_t serial;
};

#endif

