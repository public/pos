#ifndef _POS_NAMESERVER_H_
#define _POS_NAMESERVER_H_
#include <string>
#include <map>
#include <stdint.h>

class NameServer {
public:
  std::string get_name (uint32_t id);
  uint32_t get_id (std::string name);

private:
  std::map<std::string, uint32_t> cache;
};

#endif

