namespace cpp posbookie

enum E_PURCHASE_STATUS { EPS_FAIL, EPS_NOMONEY, EPS_SUCCESS }

service PosBookie {
	void ping(),

	string getSalt(),

  E_PURCHASE_STATUS
    purchaseItems(1:string auth, 2:string dataToHash, 3:list<i64> upcs),


	i32 getUPCPrice(1:i64 upc),
  i32 getStock(1:i64 upc)
}

/* POS interface :
	i32 getAccountFromHash(1:string dataToHash),
	list<string> getHashesFromAccountId(1:i32 account),
	list<string> getHashesFromAccountName(1:string account),

	i32 getHashAccountBalance(1:string dataToHash),
	i32 getIdAccountBalance(1:i32 account),
	i32 getNameAccountBalance(1:string account),

	i64 associateHashWithId(1:string auth, 2:string dataToHash, 3:i32 account),
	i64 associateHashWithName(1:string auth, 2:string dataToHash, 3:string account),
	i64 deassociateHash(1:string auth, 2:string dataToHash),

	i32 getUPCPrice(1:i64 upc),
	i64 setUPCPrice(1:string auth, 2:i64 upc, 3:i32 price),

	i64 doTransactionOnHash(1:string auth, 2:string dataToHash, 3:i32 delta),
	i64 doTransactionOnId(1:string auth, 2:i32 account, 3:i32 delta),
	i64 doTransactionOnName(1:string auth, 2:string account, 3:i32 delta),
	i64 revertTransaction(1:string auth, 2:i64 serial),

	i32 getStock(1:i64 upc),
	i64 doStockChange(1:string auth, 2:i64 upc, 3:i32 delta),

	list<string> toString()
*/

