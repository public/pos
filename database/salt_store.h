#ifndef _SALT_STORE_H_
#define _SALT_STORE_H_
#include "sha1.h"
#include <string>
#include <list>
#include <stdint.h>

class SaltStore {

public:
	SaltStore(uint64_t key_lifetime, size_t salt_size, std::string secret);
	std::string getSalt();
	bool consumeKey(std::string key);

private:
	void expireKeys();
	std::list<std::pair<uint64_t, SHA1Hash> > valid_keys;
	uint64_t key_lifetime;
	size_t salt_size;
	std::string secret;

};

#endif

