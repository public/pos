#ifndef _POS_SHA1_H_
#define _POS_SHA1_H_
#include <cstdlib>
#include <string>

struct __attribute__ ((packed)) SHA1Hash {
	SHA1Hash();
	explicit SHA1Hash(const char * str);
	explicit SHA1Hash(const std::string str);
	SHA1Hash(const SHA1Hash & other);
	SHA1Hash & operator=(const SHA1Hash & other);
	std::string toHex();
	void fromHex(std::string s);
	bool operator==(const SHA1Hash & other) const;
	void set(const unsigned char * from);
	void get(unsigned char * to) const;

	unsigned char data[20];
};

class SHA1Less {
public:
	bool operator()(const SHA1Hash & lhs, const SHA1Hash & rhs) const;
};

#endif

