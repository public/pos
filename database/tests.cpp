#include "common.h"
#include "posdb.h"
#include <iostream>

using namespace std;

int main() {
	PosDb db("db");
	uint64_t d = 0;
	cout << db.associateHash(SHA1Hash("asdfasdf"), "m4burns") << "\n";
	cout << db.getAccountFromHash(SHA1Hash("asdfasdf")) << "\n";
	cout << db.doTransaction("m4burns", 12300) << "\n";
	cout << db.doTransaction("j3parker", 23400) << "\n";
	cout << "F" << db.associateHash(SHA1Hash("asdfasdf"), "j3parker") << "\n";
	cout << db.associateHash(SHA1Hash("foobar"), "j3parker") << "\n";
	cout << db.doTransaction("m4burns", -100) << "\n";
	cout << (d = db.doTransaction("m4burns", -200)) << "\n";
	cout << db.doTransaction("m4burns", 50) << "\n";
	cout << db.doTransaction("j3parker", -23450) << "\n";
	cout << "F" << db.deassociateHash(SHA1Hash("notme")) << "\n";
	cout << db.deassociateHash(SHA1Hash("asdfasdf")) << "\n";
	cout << db.revertTransaction(d) << "\n";
	cout << "F" << db.revertTransaction(d) << "\n";
	cout << "12250? " << db.getAccountBalance("m4burns") << "\n";
	cout << "-50? " << db.getAccountBalance(SHA1Hash("foobar")) << "\n";
	uint64_t foo;
	foo.l = 12345;
	foo.h = 67890;
  cout << db.setUPCPrice(foo, db.getUPCPrice(foo) + 1) << "\n";
  cout << "price: " << db.getUPCPrice(foo) << "\n";
	cout << db.doStockChange(foo, 100) << "\n";
	cout << db.getStock(foo) << "\n";
	foo.l = 123;
	cout << db.getStock(foo) << "\n";

  cout << "\n\n\n";
  std::vector<std::string> lst = db.toString();
  for (std::vector<std::string>::iterator it = lst.begin(); it != lst.end(); it++) {
    cout << *it << "\n";
  }
	return 0;
}

